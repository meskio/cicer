# Cicer

Cicer is a web based software to manage the stock, purchases and balance of a consumer association. It has being created for the needs of the [Garbanzo Negro](http://garbanzonegro.org).

## Requirements

- Golang >= 1.12
- Node.js

## Deployment

Build a copy using make:

```shell
make build
```

Create a secret for the authentication tokens:

```shell
head -c 21 < /dev/urandom | base64
```

Now you can copy the build folder and the cicer binary to your server and run it like:

```shell
./cicer -assets 'path/to/build' -secret 'scret' -addr ':8000'
```

Instead of flags all the params can be passed as env variables. See the names between `{` and `}` in:

```shell
./cicer -h
```

### nginx configuration

The android app requires CORS to be configured in the server:

```
location / {
        if ($request_method ~ ^(OPTIONS)$ ) {
                add_header Access-Control-Allow-Origin *;
                add_header Access-Control-Allow-Methods 'GET, POST, PUT, DELETE, OPTIONS';
                add_header Access-Control-Allow-Headers x-authentication;
                return 200;
        }

        add_header Access-Control-Allow-Origin * always;
        add_header Access-Control-Allow-Methods 'GET, POST, PUT, DELETE, OPTIONS';
        add_header Access-Control-Allow-Headers x-authentication always;

        proxy_pass http://localhost:8000;
}
```

## Running it for development

To run Cicer in development mode, build and run the backend:

```shell
go build
./cicer
```

And then run the frontend with npm:

```shell
npm start
```

The backend API will be listening on port 8080, while the frontend will be on port 3000.

Open a browser and visit `http://localhost:3000`. You will be able to see any changes you do in the javascript side as you go (the backend side needs recompilation).

## Initialize data

When you run cicer, it will print an authentication token.
Use this token to seed some initial data (such a few users and products):

```shell
./setup.sh the.token.string
```

## Android

You can use sdkmanager:

```
sudo apt install sdkmanager
export ANDROID_HOME="$HOME/Android"
sdkmanager --install "platforms;android-33"
sdkmanager --install "build-tools;30.0.3"
sdkmanager --licenses
```

With it you can build the apk from command line:

```
export JAVA_HOME="$HOME/Android/android-studio/jbr"
make apk
```

The built apk will be in `android/app/build/outputs/apk/release/app-release-unsigned.apk`.

Or open android studio to test and build the app:

```
make build
npx cap copy
npx cap open android
```

### sign the apk

To sign the apk you need to have a key generated with:

```
keytool -genkey -v -keystore release.keystore -alias release -keyalg RSA -keysize 2048 -validity 10000
```

And then sign it:

```
apksigner sign --ks ~/release.keystore --out app.apk app-release-unsigned.apk
```
