class ResponseError extends Error {
  constructor(response) {
    super(response.status.toString() + " " + response.statusText);
    this.response = response;
  }
}

export { ResponseError };
