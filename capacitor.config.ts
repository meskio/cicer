import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'org.garbanzonegro.cicer',
  appName: 'Garbanzo Negro',
  webDir: 'build',
  server: {
    androidScheme: 'https'
  }
};

export default config;
