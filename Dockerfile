FROM golang as go_build

WORKDIR /src/
COPY main.go go.* /src/
COPY api /src/api

RUN CGO_ENABLED=1 go build -o cicer


FROM node as js_build

WORKDIR /src/
COPY *.json /src/
COPY src /src/src
COPY public /src/public

RUN npm install && npm run build


FROM debian

ENV ASSETS_PATH="/assets"
ENV DB_PATH="/data/cicer.db"

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y ca-certificates

COPY --from=go_build /src/cicer /cicer
COPY --from=js_build /src/build /assets

VOLUME /data

ENTRYPOINT ["/cicer"]
