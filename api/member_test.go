package api

import (
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

var (
	testMemberLogin      = "foo"
	testMemberOrderLogin = "foobar"
	testMemberAdminLogin = "bar"
)

var testMember = db.MemberReq{
	Member: db.Member{
		Num:     10,
		Login:   &testMemberLogin,
		Name:    "Foo Baz",
		Email:   "foo@example.com",
		Role:    "",
		Balance: 10000,
	},
	Password: "password",
}

var testMemberOrder = db.MemberReq{
	Member: db.Member{
		Num:     20,
		Login:   &testMemberOrderLogin,
		Name:    "FooBar Faz",
		Email:   "foobar@example.com",
		Role:    "order",
		Balance: 12000,
	},
	Password: "password",
}

var testMemberAdmin = db.MemberReq{
	Member: db.Member{
		Num:     30,
		Login:   &testMemberAdminLogin,
		Name:    "Bar Baz",
		Email:   "bar@example.com",
		Role:    "admin",
		Balance: 15000,
	},
	Password: "password",
}

func TestMemberAddList(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()

	tapi.addTestMember()
	var members []db.Member
	resp := tapi.doAdmin("GET", "/member", nil, &members)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get members:", resp.Status)
	}

	if len(members) != 3 {
		t.Fatal("Wrong number of members", len(members), members)
	}
	if members[0].Name != testMember.Name {
		t.Error("Wrong name:", members[0].Name)
	}
	if members[0].Email != "foo@example.com" {
		t.Error("Wrong email:", members[0].Email)
	}
}

func TestMemberGetDelete(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()

	resp := tapi.doAdmin("GET", "/member/10", nil, nil)
	if resp.StatusCode != http.StatusNotFound {
		t.Error("Expected not found:", resp.Status, resp.Body)
	}
	tapi.addTestMember()

	var gotMember db.Member
	resp = tapi.doAdmin("GET", "/member/10", nil, &gotMember)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if gotMember.Num != 10 {
		t.Error("Wrong member:", gotMember.Num)
	}
	resp = tapi.doAdmin("DELETE", "/member/10", nil, nil)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}

	resp = tapi.doAdmin("GET", "/member/10", nil, nil)
	if resp.StatusCode != http.StatusNotFound {
		t.Error("Expected not found after delete:", resp.Status, resp.Body)
	}
}

func TestMemberUpdate(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()

	tapi.addTestMember()
	member := testMember
	member.Role = "admin"
	resp := tapi.doAdmin("PUT", "/member/10", member, nil)
	if resp.StatusCode != http.StatusAccepted {
		t.Fatal("Can't update member:", resp.Status)
	}

	var gotMember db.Member
	resp = tapi.doAdmin("GET", "/member/10", nil, &gotMember)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if gotMember.Role != "admin" {
		t.Error("Wrong role:", gotMember)
	}
}

func TestMemberUpdateMe(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()

	tapi.addTestMember()
	member := testMember
	member.Password = "foobar"
	member.Email = "other@example.com"
	member.OldPassword = "not my password"
	resp := tapi.do("PUT", "/member/me", member, nil)
	if resp.StatusCode != http.StatusBadRequest {
		t.Error("Did accept an invalid password:", resp.Status)
	}

	member.OldPassword = testMember.Password
	resp = tapi.do("PUT", "/member/me", member, nil)
	if resp.StatusCode != http.StatusAccepted {
		t.Fatal("Can't update member:", resp.Status)
	}

	var gotMember db.Member
	resp = tapi.doAdmin("GET", "/member/10", nil, &gotMember)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if gotMember.Email != member.Email {
		t.Error("Wrong email:", gotMember)
	}
	jsonAuth := creds{
		Login:    *testMember.Login,
		Password: member.Password,
	}
	resp = tapi.do("POST", "/signin", jsonAuth, nil)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't sign in:", resp.Status)
	}
}

func (tapi *testAPI) addTestMember() {
	resp := tapi.doAdmin("POST", "/member", testMember, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create member:", resp.Status)
	}

	resp = tapi.doAdmin("POST", "/member", testMemberOrder, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create member:", resp.Status)
	}

	resp = tapi.doAdmin("POST", "/member", testMemberAdmin, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create member:", resp.Status)
	}
}
