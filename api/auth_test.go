package api

import (
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

func TestSignIn(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()

	tapi.token = ""
	resp := tapi.do("GET", "/member", nil, nil)
	if resp.StatusCode != http.StatusUnauthorized {
		t.Error("Got members without auth")
	}

	var respMember struct {
		Token  string    `json:"token"`
		Member db.Member `json:"member"`
	}
	jsonAuth := creds{
		Login:    testMemberAdminLogin,
		Password: testMemberAdmin.Password,
	}
	resp = tapi.do("POST", "/signin", jsonAuth, &respMember)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't sign in:", resp.Status)
	}
	if *respMember.Member.Login != *testMemberAdmin.Login {
		t.Fatal("Unexpected member:", respMember)
	}
	tapi.token = respMember.Token
	resp = tapi.do("GET", "/member", nil, nil)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get members:", resp.Status)
	}
}

func TestGetToken(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()

	var body struct {
		Token string `json:"token"`
	}
	resp := tapi.do("GET", "/token", nil, &body)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get token:", resp.Status)
	}

	tapi.token = body.Token
	resp = tapi.do("GET", "/transaction/mine", nil, nil)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transaction:", resp.Status)
	}
}
