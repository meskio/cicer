package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

func (a *api) AddPurchase(num int, w http.ResponseWriter, req *http.Request) {
	a.addPurchase(0, num, w, req)
}

func (a *api) AddMemberPurchase(adminNum int, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	num, _ := strconv.Atoi(vars["num"])
	a.addPurchase(adminNum, num, w, req)
}

func (a *api) addPurchase(adminNum int, memberNum int, w http.ResponseWriter, req *http.Request) {
	var purchase []db.Purchase
	err := json.NewDecoder(req.Body).Decode(&purchase)
	if err != nil {
		log.Printf("Can't create purchase: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	transaction, err := a.db.AddPurchase(adminNum, memberNum, purchase)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotAcceptable)
		} else if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusBadRequest)
		} else if errors.Is(err, db.ErrorMemberDisabled) {
			w.WriteHeader(http.StatusPaymentRequired)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("Can't create purchase: %v\n%v", err, transaction)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(transaction)
	if err != nil {
		log.Printf("Can't encode added purchase: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
