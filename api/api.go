package api

import (
	"log"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

type api struct {
	db      *db.DB
	signKey []byte
	mail    *Mail
	dues    int
}

func Init(dbPath string, signKey string, dues int, mail *Mail, r *mux.Router) error {
	database, err := db.Init(dbPath)
	if err != nil {
		return err
	}

	a := api{database, []byte(signKey), mail, dues}
	go a.refundOrders()
	go a.cleanPaswordResets()
	if dues > 0 {
		go a.checkDues()
	}

	token, err := a.newToken(0, "admin", false)
	log.Print(token)

	r.HandleFunc("/signin", a.SignIn).Methods("POST")
	r.HandleFunc("/token", a.GetToken).Methods("GET")
	r.HandleFunc("/reset", a.SendPasswordReset).Methods("POST")
	r.HandleFunc("/reset/{token}", a.ValidatePasswordReset).Methods("GET")
	r.HandleFunc("/reset/{token}", a.PasswordReset).Methods("PUT")

	r.HandleFunc("/member", a.authAdmin(a.ListMembers)).Methods("GET")
	r.HandleFunc("/member", a.authAdmin(a.AddMember)).Methods("POST")
	r.HandleFunc("/member/me", a.authNum(a.getMemberNum)).Methods("GET")
	r.HandleFunc("/member/me", a.authNum(a.UpdateMemberMe)).Methods("PUT")
	r.HandleFunc("/member/{num:[0-9]+}", a.authAdmin(a.GetMember)).Methods("GET")
	r.HandleFunc("/member/{num:[0-9]+}", a.authAdmin(a.UpdateMember)).Methods("PUT")
	r.HandleFunc("/member/{num:[0-9]+}", a.authAdmin(a.DeleteMember)).Methods("DELETE")
	r.HandleFunc("/member/{num:[0-9]+}/transactions", a.authAdmin(a.GetMemberTransactions)).Methods("GET")
	r.HandleFunc("/member/{num:[0-9]+}/purchase", a.authAdminNum(a.AddMemberPurchase)).Methods("POST")

	r.HandleFunc("/product", a.auth(a.ListProducts)).Methods("GET")
	r.HandleFunc("/product", a.authAdmin(a.AddProduct)).Methods("POST")
	r.HandleFunc("/product/{code:[0-9]+}", a.auth(a.GetProduct)).Methods("GET")
	r.HandleFunc("/product/{code:[0-9]+}", a.authAdmin(a.UpdateProduct)).Methods("PUT")
	r.HandleFunc("/product/{code:[0-9]+}", a.authAdmin(a.DeleteProduct)).Methods("DELETE")

	r.HandleFunc("/supplier", a.auth(a.ListSuppliers)).Methods("GET")
	r.HandleFunc("/supplier", a.authAdmin(a.AddSupplier)).Methods("POST")

	r.HandleFunc("/inventary", a.authAdmin(a.ListInventary)).Methods("GET")
	r.HandleFunc("/inventary/{id:[0-9]+}", a.authAdmin(a.GetInventary)).Methods("GET")
	r.HandleFunc("/inventary", a.authAdminNum(a.AddInventary)).Methods("POST")

	r.HandleFunc("/transaction", a.authAdmin(a.ListTransactions)).Methods("GET")
	r.HandleFunc("/transaction/{id:[0-9]+}", a.authNumRole(a.GetTransaction)).Methods("GET")
	r.HandleFunc("/transaction/mine", a.authNum(a.getTransactionsByMember)).Methods("GET")

	r.HandleFunc("/purchase", a.authNum(a.AddPurchase)).Methods("POST")
	r.HandleFunc("/topup", a.authAdminNum(a.AddTopup)).Methods("POST")

	r.HandleFunc("/order", a.auth(a.ListOrders)).Methods("GET")
	r.HandleFunc("/order", a.authOrderNum(a.AddOrder)).Methods("POST")
	r.HandleFunc("/order/{id:[0-9]+}", a.authNum(a.GetOrder)).Methods("GET")
	r.HandleFunc("/order/{id:[0-9]+}", a.authNumRole(a.UpdateOrder)).Methods("PUT")
	r.HandleFunc("/order/{id:[0-9]+}", a.authNumRole(a.DeleteOrder)).Methods("DELETE")
	r.HandleFunc("/order/{id:[0-9]+}/arrive", a.authNumRole(a.ArrivedOrder)).Methods("PUT")
	r.HandleFunc("/order/{id:[0-9]+}/collected", a.authNum(a.CollectOrder)).Methods("PUT")
	r.HandleFunc("/order/active", a.auth(a.ListActiveOrders)).Methods("GET")
	r.HandleFunc("/order/picks", a.authOrderNum(a.ListOrderPicks)).Methods("GET")
	r.HandleFunc("/order/unarrived", a.authNum(a.ListOrderUnarrived)).Methods("GET")
	r.HandleFunc("/order/collectable", a.authNum(a.ListOrderCollectable)).Methods("GET")
	r.HandleFunc("/order/{id:[0-9]+}/purchase", a.authNum(a.AddOrderPurchase)).Methods("POST")

	r.HandleFunc("/dues", a.authAdmin(a.ListDues)).Methods("GET")
	r.HandleFunc("/dues/mine", a.authNum(a.GetDuesByMember)).Methods("GET")
	return nil
}
