package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

type OrderGetResponse struct {
	Order       db.Order        `json:"order"`
	Transaction *db.Transaction `json:"transaction"`
}

func (a *api) refundOrders() {
	c := time.Tick(10 * time.Minute)
	for range c {
		orders := a.db.DeactivateOrders()
		for _, order := range orders {
			err := a.mail.sendOrder(order.Member.Email, &order)
			if err != nil {
				log.Println("Error sending order email:", err)
			}
		}
	}
}

func (a *api) ListOrders(w http.ResponseWriter, req *http.Request) {
	a.listOrders(false, w, req)
}

func (a *api) ListActiveOrders(w http.ResponseWriter, req *http.Request) {
	a.listOrders(true, w, req)
}

func (a *api) listOrders(active bool, w http.ResponseWriter, req *http.Request) {
	orders, err := a.db.ListOrders(active)
	if err != nil {
		log.Printf("Can't list orders: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(orders)
	if err != nil {
		log.Printf("Can't encode orders: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) ListOrderPicks(num int, w http.ResponseWriter, req *http.Request) {
	orders, err := a.db.ListOrderPicks(num)
	if err != nil {
		log.Printf("Can't list order picks: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(orders)
	if err != nil {
		log.Printf("Can't encode order picks: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) ListOrderUnarrived(num int, w http.ResponseWriter, req *http.Request) {
	orders, err := a.db.ListOrderUnarrived(num)
	if err != nil {
		log.Printf("Can't list unarrived orders: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(orders)
	if err != nil {
		log.Printf("Can't encode unarrived orders: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) ListOrderCollectable(num int, w http.ResponseWriter, req *http.Request) {
	transactions, err := a.db.ListOrderCollectable(num)
	if err != nil {
		log.Printf("Can't list collectable orders: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(transactions)
	if err != nil {
		log.Printf("Can't encode collectable orders: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) GetOrder(num int, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	order, transaction, err := a.db.GetOrder(num, id)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		log.Printf("Can't get order %s: %v", vars["id"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var body OrderGetResponse
	body.Order = order
	if transaction.ID != 0 {
		body.Transaction = &transaction
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(body)
	if err != nil {
		log.Printf("Can't encode order: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) DeleteOrder(num int, role string, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	if role == "admin" {
		num = 0
	}
	err := a.db.DeleteOrder(num, id)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		log.Printf("Can't get order %s: %v", vars["id"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (a *api) AddOrder(num int, w http.ResponseWriter, req *http.Request) {
	var order db.Order
	err := json.NewDecoder(req.Body).Decode(&order)
	if err != nil {
		log.Printf("Can't parse order: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	order.MemberNum = num
	order.Active = true
	err = a.db.AddOrder(&order)
	if err != nil {
		log.Printf("Can't create order: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(order)
	if err != nil {
		log.Printf("Can't encode order: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) UpdateOrder(num int, role string, w http.ResponseWriter, req *http.Request) {
	var order db.Order
	err := json.NewDecoder(req.Body).Decode(&order)
	if err != nil {
		log.Printf("Can't parse order: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	if role == "admin" {
		num = 0
	}
	err = a.db.UpdateOrder(num, id, &order)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		log.Printf("Can't update order %s: %v", vars["id"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	err = json.NewEncoder(w).Encode(order)
	if err != nil {
		log.Printf("Can't encode order: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) AddOrderPurchase(num int, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	var purchase []db.OrderPurchase
	err := json.NewDecoder(req.Body).Decode(&purchase)
	if err != nil {
		log.Printf("Can't parse order purchase: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(purchase) == 0 {
		log.Printf("Empty order purchase")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	transaction, err := a.db.AddOrderPurchase(num, id, purchase)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotAcceptable)
		} else if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusBadRequest)
		} else if errors.Is(err, db.ErrorMemberDisabled) {
			w.WriteHeader(http.StatusPaymentRequired)
		} else {
			log.Printf("Can't get order %d: %v", id, err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(transaction)
	if err != nil {
		log.Printf("Can't encode order transaction: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) ArrivedOrder(num int, role string, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])
	if role == "admin" {
		num = 0
	}

	err := a.db.ArrivedOrder(num, id)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		log.Printf("Can't arrive order %s: %v", vars["id"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

func (a *api) CollectOrder(num int, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, _ := strconv.Atoi(vars["id"])

	err := a.db.CollectOrder(num, id)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		log.Printf("Can't collect order %s: %v", vars["id"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}
