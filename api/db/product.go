package db

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

type Product struct {
	CreatedAt time.Time      `json:"-"`
	UpdatedAt time.Time      `json:"-"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`

	Code  int    `json:"code" gorm:"primaryKey"`
	Name  string `json:"name" gorm:"unique;index"`
	Price int    `json:"price"`
	Stock int    `json:"stock" gorm:"index"`
}

func (d *DB) AddProduct(product *Product) error {
	return d.db.Create(&product).Error
}

func (d *DB) ListProducts() (products []Product, err error) {
	err = d.db.Find(&products).Error
	return
}

func (d *DB) GetProduct(code int) (product Product, err error) {
	err = d.db.Where("code = ?", code).First(&product).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		err = ErrorNotFound
	}
	return
}

func (d *DB) DeleteProduct(code int) error {
	return d.db.Where("code = ?", code).
		Delete(&Product{}).Error
}

func (d *DB) UpdateProduct(code int, product *Product) (dbProduct Product, err error) {
	err = d.db.Where("code = ?", code).First(&dbProduct).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = ErrorNotFound
		}
		return
	}

	if product.Code != 0 {
		dbProduct.Code = product.Code
	}
	if product.Name != "" {
		dbProduct.Name = product.Name
	}
	dbProduct.Price = product.Price
	if product.Stock >= 0 {
		dbProduct.Stock = product.Stock
	}
	err = d.db.Save(&dbProduct).Error
	return
}
