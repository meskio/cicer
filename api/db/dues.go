package db

import (
	"errors"
	"time"

	"gorm.io/gorm"
)

func (d *DB) AddDuesIfNeeded(memberNum int, amount int) (transaction *Transaction, err error) {
	err = d.db.Transaction(func(tx *gorm.DB) error {
		var member Member
		err = tx.Where("num = ?", memberNum).First(&member).Error
		if err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return ErrorNotFound
			}
			return err
		}

		paid, err := areDuesPaid(tx, memberNum)
		if err != nil {
			return err
		}
		if paid {
			if member.Disabled {
				return updateMemberDisabled(tx, memberNum, false)
			}
			return nil
		}

		if member.Balance < amount {
			if !member.Disabled {
				return updateMemberDisabled(tx, memberNum, true)
			}
			return nil
		}

		transaction = &Transaction{
			MemberNum: memberNum,
			Date:      time.Now(),
			Type:      "dues",
			Total:     -amount,
		}
		err = createTransaction(tx, transaction)
		if err != nil {
			return err
		}

		if member.Disabled {
			return updateMemberDisabled(tx, memberNum, false)
		}
		return nil
	})
	return
}

func areDuesPaid(tx *gorm.DB, memberNum int) (bool, error) {
	var lastDues Transaction
	err := tx.Where("member = ? AND type = 'dues'", memberNum).
		Order("date desc").
		First(&lastDues).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil

		}
		return false, err
	}

	return lastDues.Date.Month() == time.Now().Month(), nil
}

func updateMemberDisabled(tx *gorm.DB, memberNum int, disabled bool) error {
	return tx.Model(&Member{}).
		Where("num = ?", memberNum).
		Update("disabled", disabled).Error
}

func ifDisabledError(tx *gorm.DB, memberNum int) error {
	var member Member
	err := tx.Where("num = ?", memberNum).First(&member).Error
	if err != nil {
		return err
	}
	if member.Disabled {
		return ErrorMemberDisabled
	}
	return nil
}
