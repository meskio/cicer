package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"time"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
)

type creds struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	NoExpire bool   `json:"noExpire"`
}

type passwordResetPost struct {
	Email string `json:"email"`
}

type passwordResetPut struct {
	Password string `json:"password"`
	Login    string `json:"login"`
}

func (a *api) SignIn(w http.ResponseWriter, req *http.Request) {
	var c creds
	err := json.NewDecoder(req.Body).Decode(&c)
	if err != nil {
		log.Printf("Can't decode auth credentials: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	member, err := a.db.Login(c.Login, c.Password)
	if err != nil {
		log.Printf("Invalid pass for %s: %v", c.Login, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	log.Printf("Logged in as %s", c.Login)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	token, err := a.newToken(member.Num, member.Role, !c.NoExpire)
	if err != nil {
		log.Printf("Can't create a token: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token":  token,
		"member": member})
	if err != nil {
		log.Printf("Can't encode member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) GetToken(w http.ResponseWriter, req *http.Request) {
	token := req.Header.Get("x-authentication")
	ok, claims := a.validateToken(token)
	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	numFloat, ok := claims["num"].(float64)
	if !ok {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	num := int(numFloat)

	member, err := a.db.GetMember(num)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusUnauthorized)
		} else {
			log.Printf("Can't get the member %d: %v", num, err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}
	role := member.Role

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	_, expires := claims["exp"]
	newToken, err := a.newToken(int(num), role, expires)
	if err != nil {
		log.Printf("Can't create a token: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token":    newToken,
		"role":     role,
		"disabled": member.Disabled,
	})
	if err != nil {
		log.Printf("Can't encode token: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) SendPasswordReset(w http.ResponseWriter, req *http.Request) {
	var reset passwordResetPost
	err := json.NewDecoder(req.Body).Decode(&reset)
	if err != nil {
		log.Printf("Can't decode password reset request: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	member, token, err := a.db.NewPasswordReset(reset.Email)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("Error creating password reset: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = a.mail.sendPasswordReset(member, "/reset/"+token)
	if err != nil {
		log.Printf("Error sending password reset: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Email sent"))
}

func (a *api) ValidatePasswordReset(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	token := vars["token"]
	passwordReset, err := a.db.GetPasswordReset(token)
	if err != nil {
		log.Printf("Can't get password reset %s: %v", token, err)
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(*passwordReset.Member)
	if err != nil {
		log.Printf("Can't encode member: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) PasswordReset(w http.ResponseWriter, req *http.Request) {
	var reset passwordResetPut
	err := json.NewDecoder(req.Body).Decode(&reset)
	if err != nil {
		log.Printf("Can't decode password reset put: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(req)
	token := vars["token"]
	err = a.db.ResetPassword(token, reset.Password, reset.Login)
	if err != nil {
		log.Printf("Can't reset password %s: %v", token, err)
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte("Email sent"))
}

func (a *api) cleanPaswordResets() {
	c := time.Tick(10 * time.Minute)
	for range c {
		a.db.CleanPasswordReset()
	}
}

func (a *api) auth(fn func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		if ok, _ := a.validateToken(token); !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(w, req)
	}
}

func (a *api) authNum(fn func(int, http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		ok, claims := a.validateToken(token)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		num, ok := claims["num"].(float64)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(int(num), w, req)
	}
}

func (a *api) authAdmin(fn func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		ok, claims := a.validateToken(token)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		role, ok := claims["role"].(string)
		if !ok || role != "admin" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(w, req)
	}
}

func (a *api) authAdminNum(fn func(int, http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		ok, claims := a.validateToken(token)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		role, ok := claims["role"].(string)
		if !ok || role != "admin" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		num, ok := claims["num"].(float64)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(int(num), w, req)
	}
}

func roleOrder(role string) bool {
	return role == "admin" || role == "order"
}

func (a *api) authOrder(fn func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		ok, claims := a.validateToken(token)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		role, ok := claims["role"].(string)
		if !ok || !roleOrder(role) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(w, req)
	}
}

func (a *api) authOrderNum(fn func(int, http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		ok, claims := a.validateToken(token)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		role, ok := claims["role"].(string)
		if !ok || !roleOrder(role) {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		num, ok := claims["num"].(float64)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(int(num), w, req)
	}
}

func (a *api) authNumRole(fn func(int, string, http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("x-authentication")
		ok, claims := a.validateToken(token)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		num, ok := claims["num"].(float64)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		role, ok := claims["role"].(string)
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		fn(int(num), role, w, req)
	}
}

func (a *api) newToken(num int, role string, expire bool) (string, error) {
	claims := jwt.MapClaims{
		"num":  num,
		"role": role,
	}
	if expire {
		claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(a.signKey)
}

func (a *api) validateToken(token string) (bool, jwt.MapClaims) {
	t, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return a.signKey, nil
	})
	if err != nil {
		return false, nil
	}
	if !t.Valid {
		return false, nil
	}
	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		return false, nil
	}
	expClaim, ok := claims["exp"]
	if !ok {
		return true, claims
	}
	exp, ok := expClaim.(float64)
	if !ok {
		return false, claims
	}
	return time.Unix(int64(exp), 0).After(time.Now()), claims
}
