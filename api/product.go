package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"0xacab.org/meskio/cicer/api/db"
	"github.com/gorilla/mux"
)

func (a *api) AddProduct(w http.ResponseWriter, req *http.Request) {
	var product db.Product
	err := json.NewDecoder(req.Body).Decode(&product)
	if err != nil {
		log.Printf("Can't create product: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = a.db.AddProduct(&product)
	if err != nil {
		log.Printf("Can't create product: %v\n%v", err, product)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(product)
	if err != nil {
		log.Printf("Can't encode added product: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) ListProducts(w http.ResponseWriter, req *http.Request) {
	products, err := a.db.ListProducts()
	if err != nil {
		log.Printf("Can't list products: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(products)
	if err != nil {
		log.Printf("Can't encode products: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) GetProduct(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	code, _ := strconv.Atoi(vars["code"])
	product, err := a.db.GetProduct(code)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		log.Printf("Can't get product %d: %v", code, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(product)
	if err != nil {
		log.Printf("Can't encode product: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (a *api) DeleteProduct(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	code, _ := strconv.Atoi(vars["code"])
	err := a.db.DeleteProduct(code)
	if err != nil {
		log.Printf("Can't delete product %s: %v", vars["code"], err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (a *api) UpdateProduct(w http.ResponseWriter, req *http.Request) {
	var product db.Product
	err := json.NewDecoder(req.Body).Decode(&product)
	if err != nil {
		log.Printf("Can't decode product: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(req)
	code, _ := strconv.Atoi(vars["code"])
	dbProduct, err := a.db.UpdateProduct(code, &product)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		log.Printf("Can't update product %s: %v %v", vars["code"], err, product)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	err = json.NewEncoder(w).Encode(dbProduct)
	if err != nil {
		log.Printf("Can't encode updated product: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
