package api

import (
	"fmt"
	"net/http"
	"path"
	"strconv"
	"testing"
	"time"

	"0xacab.org/meskio/cicer/api/db"
)

var testOrder = db.Order{
	Name:        "huevos",
	Description: "huevos frescos",
	Deadline:    time.Now().Add(24 * time.Hour),
	Products: []db.OrderProduct{
		{
			ProductCode: testProduct.Code,
			Price:       testProduct.Price,
		},
	},
}

func TestOrderAddList(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	if len(orders) != 1 {
		t.Fatal("Wrong number of orders", len(orders), orders)
	}
	if orders[0].Name != testOrder.Name {
		t.Error("Wrong name:", orders[0].Name)
	}
	if len(orders[0].Products) != 1 {
		t.Fatal("Wrong number of products", len(orders[0].Products), orders[0].Products)
	}
	if orders[0].Products[0].Price != testProduct.Price {
		t.Error("Wrong product price:", orders[0].Products[0].Price)
	}
}

func TestOrderActive(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	if len(orders) != 1 {
		t.Fatal("Wrong number of orders", len(orders), orders)
	}
	if orders[0].Name != testOrder.Name {
		t.Error("Wrong name:", orders[0].Name)
	}
}

func TestOrderDelete(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	order := testOrder
	order.Deadline = time.Now().Add(-24 * time.Hour)
	resp := tapi.doOrder("POST", "/order", order, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.do("GET", "/order", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.doAdmin("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order purchase:", resp.Status)
	}

	orders = tapi.deactivateOrders()
	if len(orders) != 1 {
		t.Error("Deactivated wrong orders:", orders)
	}

	resp = tapi.doOrder("DELETE", fmt.Sprintf("/order/%d", orders[0].ID), nil, nil)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't delete order:", resp.Status)
	}

	resp = tapi.do("GET", "/order", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if len(orders) != 0 {
		t.Error("The order didn't get removed", orders)
	}

	var transactions []db.Transaction
	resp = tapi.doAdmin("GET", "/transaction", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if len(transactions) != 0 {
		t.Error("The transactions didn't get removed", transactions)
	}

	var member db.Member
	resp = tapi.doAdmin("GET", fmt.Sprintf("/member/%d", testMember.Num), nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if member.Balance != testMember.Balance {
		t.Error("Wrong balance on test member:", member.Balance)
	}

	resp = tapi.doAdmin("GET", fmt.Sprintf("/member/%d", testMemberAdmin.Num), nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if member.Balance != testMemberAdmin.Balance {
		t.Error("Wrong balance on admin member:", member.Balance)
	}
}

func TestOrderPurchase(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	total := 3 * testProduct.Price
	if transactions[0].Total != -total {
		t.Fatal("Wrong total", transactions[0].Total)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-total {
		t.Error("Wrong product balance:", member.Balance)
	}
}

func TestOrderNoDeactivation(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	order := testOrder
	now := time.Now()
	order.Deadline = now.Add(time.Hour)
	resp := tapi.doOrder("POST", "/order", order, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(orders) != 1 {
		t.Fatal("Didn't find my new order")
	}

	orders = tapi.deactivateOrders()
	if len(orders) != 0 {
		t.Error("Deactivated some orders:", orders)
	}

	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(orders) != 1 {
		t.Fatal("Didn't find my new order after deactivation")
	}
}

func TestOrderDeactivation(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	order := testOrder
	order.Deadline = time.Now().Add(-1 * time.Minute).UTC()
	resp := tapi.doOrder("POST", "/order", order, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.doOrder("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(orders) != 1 {
		t.Fatal("Didn't find my new order")
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.doAdmin("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	dbPath := path.Join(tapi.testPath, "test.db")
	database, err := db.Init(dbPath)
	if err != nil {
		t.Fatal("Can't initialize the db:", err)
	}
	orders = database.DeactivateOrders()
	if len(orders) != 1 {
		t.Error("Deactivated wrong orders:", orders)
	}

	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(orders) != 0 {
		t.Fatal("I found some orders")
	}

	total := 3 * testProduct.Price

	var transactions []db.Transaction
	resp = tapi.doOrder("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	if transactions[0].Type != "refund" {
		t.Fatal("Should be a refund", transactions[0].Type)
	}
	if transactions[0].Total != total {
		t.Fatal("Wrong total:", transactions[0].Total)
	}

	var member db.Member
	resp = tapi.doOrder("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't member:", resp.Status)
	}
	if member.Balance != testMemberOrder.Balance+total {
		t.Fatal("Wrong member balance:", member.Balance, testMemberOrder.Balance, total)
	}
}

func TestGetOrder(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if len(orders) != 1 {
		t.Fatal("Didn't find my new order")
	}
	id := strconv.Itoa(int(orders[0].ID))

	var body OrderGetResponse
	resp = tapi.do("GET", "/order/"+id, nil, &body)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get order:", resp.Status)
	}
	if body.Transaction != nil {
		t.Error("Unexpected transaction", body.Transaction)
	}
	if body.Order.Name != testOrder.Name {
		t.Error("Wrong name:", body.Order.Name)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	resp = tapi.do("GET", "/order/"+id, nil, &body)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get order:", resp.Status)
	}
	if body.Transaction == nil {
		t.Fatal("There is no transaction")
	}
	if body.Transaction.Type != "order" {
		t.Error("Wrong transaction type", body.Transaction.Type)
	}
}

func TestUpdateOrderPurchase(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	purchase[0].Amount = 2
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	total := 2 * testProduct.Price
	if transactions[0].Total != -total {
		t.Fatal("Wrong total", transactions[0].Total)
	}
	if transactions[0].OrderPurchase[0].Amount != 2 {
		t.Fatal("Wrong amount", transactions[0].OrderPurchase)
	}

	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if len(orders[0].Transactions[0].OrderPurchase) != 1 {
		t.Fatal("Wrong number of product purchases:", orders[0].Transactions[0].OrderPurchase)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-total {
		t.Error("Wrong product balance:", member.Balance)
	}
}

func TestOrderUpdate(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	order := testOrder
	order.Products = make([]db.OrderProduct, len(testOrder.Products))
	copy(order.Products, testOrder.Products)
	order.Products[0].Price = 1000
	resp = tapi.doOrder("PUT", fmt.Sprintf("/order/%d", orders[0].ID), order, nil)
	if resp.StatusCode != http.StatusAccepted {
		tapi.t.Fatal("Can't update order:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	total := 3 * order.Products[0].Price
	if transactions[0].Total != -total {
		t.Error("Wrong total", transactions[0].Total)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-total {
		t.Error("Wrong product balance:", member.Balance)
	}
}

func TestOrderUpdateProduct(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	order := testOrder
	order.Products = make([]db.OrderProduct, len(testOrder.Products))
	copy(order.Products, testOrder.Products)
	order.Products[0].Price = testProduct2.Price
	order.Products[0].ProductCode = testProduct2.Code
	resp = tapi.doOrder("PUT", fmt.Sprintf("/order/%d", orders[0].ID), order, nil)
	if resp.StatusCode != http.StatusAccepted {
		tapi.t.Fatal("Can't update order:", resp.Status)
	}

	var orderResponse OrderGetResponse
	resp = tapi.do("GET", fmt.Sprintf("/order/%d", orders[0].ID), nil, &orderResponse)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get order:", resp.Status)
	}
	if len(orderResponse.Order.Products) != 1 {
		t.Fatal("Wrong len of products:", orderResponse.Order.Products)
	}
	if orderResponse.Order.Products[0].ProductCode != testProduct2.Code {
		t.Fatal("Wrong product code:", orderResponse.Order.Products)
	}
	if orderResponse.Order.Products[0].Price != testProduct2.Price {
		t.Fatal("Wrong product price:", orderResponse.Order.Products)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	if transactions[0].Total != 0 {
		t.Error("Wrong total", transactions[0].Total)
	}
	if len(transactions[0].OrderPurchase) != 0 {
		t.Error("Wrong purchases", transactions[0].OrderPurchase)
	}
}

func TestOrderUpdateAddProduct(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestOrder()

	var orders []db.Order
	resp := tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	order := testOrder
	order.Products = make([]db.OrderProduct, len(testOrder.Products))
	copy(order.Products, testOrder.Products)
	order.Products = append(order.Products, db.OrderProduct{
		ProductCode: testProduct2.Code,
		Price:       testProduct2.Price,
	})
	resp = tapi.doOrder("PUT", fmt.Sprintf("/order/%d", orders[0].ID), order, nil)
	if resp.StatusCode != http.StatusAccepted {
		tapi.t.Fatal("Can't update order:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	expectedTotal := -(order.Products[0].Price * 3)
	if transactions[0].Total != expectedTotal {
		t.Error("Wrong total", transactions[0].Total, expectedTotal)
	}
}

func TestOrderUpdateDelProduct(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()

	order := testOrder
	order.Products = make([]db.OrderProduct, len(testOrder.Products))
	copy(order.Products, testOrder.Products)
	order.Products = append(order.Products, db.OrderProduct{
		ProductCode: testProduct2.Code,
		Price:       testProduct2.Price,
	})
	resp := tapi.doOrder("POST", "/order", order, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
		{
			OrderProductID: orders[0].Products[1].ID,
			Amount:         2,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	expectedTotal := -(order.Products[0].Price*3 + order.Products[1].Price*2)
	if transactions[0].Total != expectedTotal {
		t.Error("Wrong total", transactions[0].Total, expectedTotal)
	}
	if len(transactions[0].OrderPurchase) != 2 {
		t.Error("Wrong purchases", transactions[0].OrderPurchase)
	}

	order.Products = []db.OrderProduct{order.Products[0]}
	resp = tapi.doOrder("PUT", fmt.Sprintf("/order/%d", orders[0].ID), order, nil)
	if resp.StatusCode != http.StatusAccepted {
		tapi.t.Fatal("Can't update order:", resp.Status)
	}

	var orderResponse OrderGetResponse
	resp = tapi.do("GET", fmt.Sprintf("/order/%d", orders[0].ID), nil, &orderResponse)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get order:", resp.Status)
	}
	if len(orderResponse.Order.Products) != 1 {
		t.Fatal("Wrong len of products:", len(orderResponse.Order.Products), orderResponse.Order.Products)
	}
	if orderResponse.Order.Products[0].ProductCode != testProduct.Code {
		t.Error("Wrong product code:", orderResponse.Order.Products)
	}
	if orderResponse.Order.Products[0].Price != testProduct.Price {
		t.Error("Wrong product price:", orderResponse.Order.Products)
	}

	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(orders), orders)
	}
	if transactions[0].Total != -testProduct.Price*3 {
		t.Error("Wrong total", transactions[0].Total)
	}
	if len(transactions[0].OrderPurchase) != 1 {
		t.Error("Wrong purchases", transactions[0].OrderPurchase)
	}
}

func TestOrderUpdateReactivate(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	order := testOrder
	now := time.Now()
	order.Deadline = time.Date(now.Year(), now.Month(), now.Day()-1, 0, 0, 0, 0, time.Local)
	resp := tapi.doOrder("POST", "/order", order, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	orders = tapi.deactivateOrders()
	if len(orders) != 1 {
		t.Error("Deactivated none orders:", orders)
	}

	order.Deadline = testOrder.Deadline
	resp = tapi.doOrder("PUT", fmt.Sprintf("/order/%d", orders[0].ID), order, nil)
	if resp.StatusCode != http.StatusAccepted {
		tapi.t.Fatal("Can't update order:", resp.Status)
	}

	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}
	if len(orders) != 1 {
		t.Error("The order is not being reactivated", orders)
	}

	var transactions []db.Transaction
	resp = tapi.doOrder("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 0 {
		t.Fatal("Wrong number of transactions", transactions)
	}
}

func TestOrderUpdateDeactivated(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	order := testOrder
	now := time.Now()
	order.Deadline = time.Date(now.Year(), now.Month(), now.Day()-1, 0, 0, 0, 0, time.Local)
	resp := tapi.doOrder("POST", "/order", order, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.do("GET", "/order/active", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders:", resp.Status)
	}

	purchase := []db.OrderPurchase{
		{
			OrderProductID: orders[0].Products[0].ID,
			Amount:         3,
		},
	}
	resp = tapi.do("POST", fmt.Sprintf("/order/%d/purchase", orders[0].ID), purchase, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create order:", resp.Status)
	}

	orders = tapi.deactivateOrders()
	if len(orders) != 1 {
		t.Error("Deactivated none orders:", orders)
	}

	order.Products[0].Price = 1000
	resp = tapi.doOrder("PUT", fmt.Sprintf("/order/%d", orders[0].ID), order, nil)
	if resp.StatusCode != http.StatusAccepted {
		tapi.t.Fatal("Can't update order:", resp.Status)
	}

	total := 3 * order.Products[0].Price
	var transactions []db.Transaction
	resp = tapi.doOrder("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", transactions)
	}
	if transactions[0].Total != total {
		t.Fatal("Wrong updated total", transactions[0].Total, total)
	}
}

func TestOrderPicks(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()
	tapi.addTestOrder()

	testOrderOld := testOrder
	testOrderOld.Deadline = time.Now().Add(-24 * time.Hour)
	resp := tapi.doOrder("POST", "/order", testOrderOld, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create old order:", resp.Status)
	}

	var orders []db.Order
	resp = tapi.doOrder("GET", "/order/picks", nil, &orders)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get orders picks:", resp.Status)
	}

	if len(orders) != 1 {
		t.Fatal("Wrong number of orders", len(orders), orders)
	}
	if orders[0].Name != testOrder.Name {
		t.Error("Wrong name:", orders[0].Name)
	}
	if orders[0].Deadline.Day() != testOrder.Deadline.Day() {
		t.Fatal("Wrong deadline", orders[0].Deadline, "!=", testOrder.Deadline)
	}
}

func (tapi *testAPI) addTestOrder() {
	resp := tapi.doOrder("POST", "/order", testOrder, nil)
	if resp.StatusCode != http.StatusCreated {
		tapi.t.Fatal("Can't create order:", resp.Status)
	}
}

func (tapi *testAPI) deactivateOrders() []db.Order {
	dbPath := path.Join(tapi.testPath, "test.db")
	database, err := db.Init(dbPath)
	if err != nil {
		tapi.t.Fatal("Can't initialize the db:", err)
	}
	return database.DeactivateOrders()
}
