package api

import (
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

func TestTopupAddListMine(t *testing.T) {
	tapi := newTestAPI(t)
	defer tapi.close()
	tapi.addTestMember()
	tapi.addTestProducts()

	topup := map[string]interface{}{
		"member":  testMember.Num,
		"comment": "my topup",
		"amount":  20,
	}
	resp := tapi.doAdmin("POST", "/topup", topup, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create topup:", resp.Status)
	}
	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}

	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}
	if transactions[0].Total != 20 {
		t.Error("Wrong total:", transactions[0].Total)
	}
	if transactions[0].Proxy.Num != testMemberAdmin.Num {
		t.Error("Wrong proxy:", transactions[0].Proxy.Num)
	}
	if transactions[0].Topup.Comment != "my topup" {
		t.Error("Wrong topup comment:", transactions[0].Topup.Comment)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance+20 {
		t.Error("Wrong product balance:", member.Balance)
	}
}
