package api

import (
	"net/http"
	"testing"

	"0xacab.org/meskio/cicer/api/db"
)

func TestListDues(t *testing.T) {
	const dues = 500
	tapi := newTestAPIDues(t, dues)
	defer tapi.close()
	tapi.addTestMember()

	topup := map[string]interface{}{
		"member":  testMember.Num,
		"comment": "chargme dues",
		"amount":  0,
	}
	resp := tapi.doAdmin("POST", "/topup", topup, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create topup:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/dues/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}
	if transactions[0].Type != "dues" {
		t.Error("Wrong type:", transactions[0].Type)
	}
	if transactions[0].Total != -dues {
		t.Error("Wrong total:", transactions[0].Total)
	}
}

func TestChargeDuesOnce(t *testing.T) {
	const dues = 500
	tapi := newTestAPIDues(t, dues)
	defer tapi.close()
	tapi.addTestMember()

	topup := map[string]interface{}{
		"member":  testMember.Num,
		"comment": "chargme dues",
		"amount":  0,
	}
	resp := tapi.doAdmin("POST", "/topup", topup, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create topup:", resp.Status)
	}

	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 2 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}
	if transactions[0].Type != "dues" {
		t.Error("Wrong type:", transactions[0].Type)
	}
	if transactions[0].Total != -dues {
		t.Error("Wrong total:", transactions[0].Total)
	}

	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-dues {
		t.Error("Wrong product balance:", member.Balance)
	}

	// If dues are already payed they should not be charged a second time
	resp = tapi.doAdmin("POST", "/topup", topup, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create topup:", resp.Status)
	}

	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 3 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}

	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != testMember.Balance-dues {
		t.Error("Wrong member balance after dues:", member.Balance)
	}
}

func TestMemberIsDisabled(t *testing.T) {
	const dues = 500
	tapi := newTestAPIDues(t, dues)
	defer tapi.close()
	tapi.addTestMember()

	topup := map[string]interface{}{
		"member":  testMember.Num,
		"comment": "empty my balance",
		"amount":  -testMember.Balance,
	}
	resp := tapi.doAdmin("POST", "/topup", topup, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create topup:", resp.Status)
	}

	// check that the dues hasn't being added as transaction
	var transactions []db.Transaction
	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 1 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}

	// check that the member is disabled
	var member db.Member
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != 0 {
		t.Error("Wrong member balance:", member.Balance)
	}
	if !member.Disabled {
		t.Error("Member is not disabled")
	}

	// pay dues
	topup["amount"] = dues
	resp = tapi.doAdmin("POST", "/topup", topup, nil)
	if resp.StatusCode != http.StatusCreated {
		t.Fatal("Can't create topup:", resp.Status)
	}

	resp = tapi.do("GET", "/transaction/mine", nil, &transactions)
	if resp.StatusCode != http.StatusOK {
		t.Fatal("Can't get transactions:", resp.Status)
	}
	if len(transactions) != 3 {
		t.Fatal("Wrong number of transactions", len(transactions), transactions)
	}

	// check that the member is reenabled
	resp = tapi.do("GET", "/member/me", nil, &member)
	if resp.StatusCode != http.StatusOK {
		t.Error("Can't find the member:", resp.Status)
	}
	if member.Balance != 0 {
		t.Error("Wrong member balance:", member.Balance)
	}
	if member.Disabled {
		t.Error("Member is still disabled")
	}
}
