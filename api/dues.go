package api

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

func (a *api) ListDues(w http.ResponseWriter, req *http.Request) {
	a.GetDuesByMember(0, w, req)
}

func (a *api) GetDuesByMember(num int, w http.ResponseWriter, req *http.Request) {
	query := map[string][]string{"type": []string{"dues"}}
	transactions, err := a.db.ListTransactions(num, query)
	if err != nil {
		log.Printf("Can't list dues: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(transactions)
	if err != nil {
		log.Printf("Can't encode dues: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (a *api) checkDues() {
	c := time.Tick(time.Minute)
	for range c {
		members, err := a.db.ListMembers()
		if err != nil {
			log.Println("Error listing members to check dues:", err)
			continue
		}
		for _, member := range members {
			_, err := a.db.AddDuesIfNeeded(member.Num, a.dues)
			if err != nil {
				log.Println("Error charging dues to member", member.Num, ":", err)
			}
		}
	}
}
