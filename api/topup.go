package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"0xacab.org/meskio/cicer/api/db"
)

type TopupReq struct {
	Member  int    `json:"member"`
	Comment string `json:"comment"`
	Amount  int    `json:"amount"`
}

func (a *api) AddTopup(adminNum int, w http.ResponseWriter, req *http.Request) {
	var topup TopupReq
	err := json.NewDecoder(req.Body).Decode(&topup)
	if err != nil {
		log.Printf("Can't parse topup: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	transaction, err := a.db.AddTopup(adminNum, topup.Member, topup.Amount, topup.Comment)
	if err != nil {
		if errors.Is(err, db.ErrorNotFound) {
			w.WriteHeader(http.StatusNotAcceptable)
		} else if errors.Is(err, db.ErrorInvalidRequest) {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("Can't create topup: %v\n%v", err, transaction)
		}
		return
	}
	if a.dues != 0 {
		_, err := a.db.AddDuesIfNeeded(topup.Member, a.dues)
		if err != nil {
			log.Println("Error charging dues to member", topup.Member, ":", err)
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(transaction)
	if err != nil {
		log.Printf("Can't encode added topup: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

}
