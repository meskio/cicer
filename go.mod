module 0xacab.org/meskio/cicer

go 1.22

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.1
	github.com/olivere/env v1.1.0
	golang.org/x/crypto v0.24.0
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.1 // FIXME: newer versions fail transaction.order
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.22 // indirect
	golang.org/x/sys v0.21.0 // indirect
)
